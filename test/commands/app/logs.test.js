const {expect, test} = require('@oclif/test')

describe('app:logs', () => {
  test
  .stdout()
  .command(['app:logs', '--env', 'production'])
  .it('runs logs --env production', ctx => {
    expect(ctx.stdout).to.contain('production')
  })

  test
  .stdout()
  .command(['app:logs', '--env', 'production', '--app', 'app'])
  .it('runs logs --env production --app app', ctx => {
    expect(ctx.stdout).to.contain('app')
  })

  test
  .stdout()
  .command(['app:logs', '--env', 'production', '--image', 'image'])
  .it('runs logs --env production --image "image"', ctx => {
    expect(ctx.stdout).to.contain('image')
  })

  test
  .stdout()
  .command(['app:logs', '--env', 'production',  '--live'])
  .it('runs logs --env production --live"', ctx => {
    expect(ctx.stdout).to.contain('live')
  })

  test
  .stdout()
  .command(['app:logs', '--env', 'production', '--field', 'container.id=xyz'])
  .it('runs logs --env --field container.id=xyz', ctx => {
    expect(ctx.stdout).to.contain('container.id=xyz')
  })
})
