const {Command, flags} = require('@oclif/command')
const axios = require('axios')
const {cli} = require('cli-ux')

const USER = process.env.USER
const PASSWORD = process.env.PASSWORD
const ADDR = process.env.ADDR
const PORT = process.env.PORT || 9243
const SEARCH_PATH = process.env.SEARCH_PATH || '/filebeat*/_search'

/* eslint-disable no-console */
class LogsCommand extends Command {
  async checkExistanceEnvVariable() {
    let err = 'You must provide a complete url access with its credentials!  USER | PASSWORD | ADDR'

    if ((!USER) || (!PASSWORD) || (!ADDR)) {
      throw (err)
    }
  }

  async filtering() {
    let url = 'https://' + USER + ':' + PASSWORD + '@' + ADDR + ':' + PORT + SEARCH_PATH

    let data =
      {query: {bool: {must: []}}}

    let timeout =
      {timeout: 4096}

    if (LogsCommand.hashmap.env)
      data.query.bool.must.push({match: {'docker.container.labels.environment': LogsCommand.hashmap.env}})

    if (LogsCommand.hashmap.app)
      data.query.bool.must.push({match: {'docker.container.labels.app': LogsCommand.hashmap.app}})

    if (LogsCommand.hashmap.image)
      data.query.bool.must.push({match: {'container.image.name': LogsCommand.hashmap.image}})

    if (LogsCommand.hashmap.field) {
      let queryData = LogsCommand.hashmap.field.split('=')
      let field = {}
      field[queryData[0]] = queryData[1]
      if (queryData.length === 2) {
        data.query.bool.must.push({match: field})
      }
    }

    cli.action.start('Fetching data ...', {stdout: true})
    await axios.post(url, data, timeout)
    .then(response => {
      console.dir(JSON.parse(JSON.stringify(response.data)), {depth: null, colors: true})
    })
    .catch(error => {
      console.error(error.code)
      console.error(error.message)
      console.error(error.stack)
    })
    cli.action.stop()
  }

  async run() {
    this.checkExistanceEnvVariable()

    const {flags} = this.parse(LogsCommand)

    if (flags.env) {
      console.info(`--env is is set with ${flags.env}`)
      LogsCommand.hashmap.env = flags.env
    }

    if (flags.app) {
      console.info(`--app is set with ${flags.app}`)
      LogsCommand.hashmap.app = flags.app
    }

    if (flags.image) {
      console.info(`--image is set with ${flags.image}`)
      LogsCommand.hashmap.image = flags.image
    }

    if (flags.field) {
      console.info(` --field is ${flags.field}`)
      LogsCommand.hashmap.field = flags.field
    }

    if (flags.live) {
      console.info('--live is toogle on')
      LogsCommand.hashmap.live = flags.live
    }
    this.filtering()
  }
}

LogsCommand.description = 'CLI - Get Elastic Search Logs with filter options'

LogsCommand.flags = {
  env: flags.string({
    char: 'e',
    required: true,
    description: 'Filter by docker.container.labels.environment',
  }),
  app: flags.string({
    char: 'a',
    required: false,
    description: 'Filter by docker.container.labels.app',
  }),
  image: flags.string({
    char: 'c',
    required: false,
    description: 'Filter by container.image.name',
  }),
  live: flags.boolean({
    char: 't',
    required: false,
    description: 'Toggle on live logs',
  }),
  field: flags.string({
    char: 'f',
    required: false,
    description: 'Filter by custom field. Example: container.id=xyz',
  }),
}

LogsCommand.hashmap = {
  env: '',
  app: '',
  image: '',
  field: '',
  live: false,
}

module.exports = LogsCommand
