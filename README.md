# elastic-logs-cli

## CLI Interface for getting logs from elastic search with filter options

### Filter by 
  * docker.container.labels.environment 
  * docker.container.labels.app 
  * container.image.name 

###  by any other field: 
  * container.id = xyz

### by live live stream

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/elastic-logs-cli.svg)](https://npmjs.org/package/elastic-logs-cli)
[![Downloads/week](https://img.shields.io/npm/dw/elastic-logs-cli.svg)](https://npmjs.org/package/elastic-logs-cli)
[![License](https://img.shields.io/npm/l/elastic-logs-cli.svg)](https://github.com/edsonma/elastic-logs-cli/blob/master/package.json)

<!-- toc -->
* [elastic-logs-cli](#elastic-logs-cli)
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
 <!-- usage -->
```sh-session
$ npm install -g elastic-logs-cli
$ elastic-logs COMMAND
running command...
$ elastic-logs (-v|--version|version)
elastic-logs-cli/0.0.0 darwin-x64 node-v12.11.1
$ elastic-logs --help [COMMAND]
USAGE
  $ elastic-logs COMMAND

ENV VARIABLES
   - USER  (* required)
   - PASSWORD (* required)
   - ADDR (* required)
   - PORT (default: 9243 - optional)      
   - SEARCH_PATH (default: /filebeat*/_search - optional)

OPTIONS
  -e, --env, Filter by docker.container.labels.environment
  -a, --app, Filter by docker.container.labels.app
  -c, --image, Filter by container.image.name
  -f, --feild, Filter by custom field: Ex. container.id = yes
  -t, --live, straming of logs
```
<!-- usagestop -->
# Commands
## `elastic-logs app:logs`

* [`elastic-logs help [COMMAND]`](#elastic-logs-help-command)


